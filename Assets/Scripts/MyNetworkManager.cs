﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MyNetworkManager : NetworkManager 
{
	[SerializeField]
	[Range(1,10)]
	private float r = 5f;

	public override void OnServerAddPlayer (NetworkConnection conn, short playerControllerId)
	{
		Vector3 pos = GetRandomPosition();

		var player = Instantiate(playerPrefab, pos, Quaternion.identity) as GameObject;

		NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
	}

	public Vector3 GetRandomPosition()
	{
		var point = Random.insideUnitCircle * r;

		return new Vector3(point.x, 0, point.y);
	}
}